#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>

static FILE* f_in = NULL;
static char* filename_out = NULL;
static size_t size_filename_out = 0;

/* reverse: переворачивает строку s (результат в s) */
/* Модифицированный вариант функции reverse() из книги "Язык */
/* программирования Си" Кернигана и Ритчи */
char* reverse(char* s)
{
	char c;
	size_t i, j;
	for (i = 0, j = strlen(s)-1; i < j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
	
	return s;
}

/* longint2str: преобразование n в строку s */
/* Модифицированный вариант функции itoa() из книги "Язык */
/* программирования Си" Кернигана и Ритчи */
/* Возвращает длину итоговой строки. */
/* s может быть равно NULL, в этом случае преобразование */
/* не производится, а только возвращается длина строки. */
size_t longint2str(long int n, char* s) 
{ 
	char digits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	long int sign; 
	
	if ((sign = n) < 0) /* сохраняем знак */ 
		n = -n; /* делаем n положительным */ 
	
	size_t i = 0; 
	
	do { /* генерируем цифры в обратном порядке */
		if (s != NULL)
			s[i] = digits[n % 10]; /* следующая цифра */ 
		++i;
	} while ((n /= 10) > 0); /* исключить ее */ 
	
	if (sign < 0) {
		if (s != NULL)
			s[i] = '-';
		++i;
	}
	
	if (s != NULL) {
		s[i] = '\0'; 
		reverse(s); 
	}
	
	return i;
}

/* str2long: преобразование строки s в long int */
/* В случае ошибки возвращает 0 (ноль)! */
long int str2longint(const char* s)
{
	long int result = strtol(s, NULL, 0);
	if (result == 0 || (errno == ERANGE && (result == LONG_MIN || result == LONG_MAX)))
		return 0;

	return result;
}

/* Cleanup function */
void cleanup()
{
	if (f_in != NULL) {
		fclose(f_in);
		f_in = NULL;
	}
	
	if (filename_out != NULL) {
		free(filename_out);
		filename_out = NULL;
	}
}

/* Generate output file name in format "<input-file-name>.<part-number>" */
void generate_filename_out(const char* fname_in, long int part_num)
{
	const size_t ReservedBytes = 8;

	size_t len_part_num = longint2str(part_num, NULL);
	size_t len_fname_in = strlen(fname_in);

	size_t new_size = len_fname_in + 1 /* '.' */ + len_part_num + 1 /* '\0' */;
	if (new_size > size_filename_out) {
		if (filename_out != NULL) 
			free(filename_out);
		new_size += ReservedBytes; // to avoid future memory allocation
		filename_out = (char*)malloc(new_size);
		if (filename_out == NULL) {
			printf("Error allocating memory! (size = %d byte(s))\n", new_size);
			cleanup();
			exit(EXIT_FAILURE);
		}
		size_filename_out = new_size;
	}

	strcpy(filename_out, fname_in);
	char* p = filename_out + len_fname_in;
	*p = '.';
	p++;
	longint2str(part_num, p);
}

/* Copy part_size bytes from file f_in to new file with name fname_out */
void makepart(FILE* f_in, const char* fname_out, long int part_size)
{

	enum { BUFFER_SIZE = 16 * 1024 };
	static char buf[BUFFER_SIZE];

	FILE* f_out = fopen(fname_out, "wb");
	if (f_out == NULL) {
		printf("Cannot open file \"%s\"\n", fname_out);
		perror("Error information:");
		cleanup();
		exit(EXIT_FAILURE);
	}

	int need_read_all = (part_size == 0);
	size_t total_written_bytes = 0;
	size_t num_bytes_to_read = 0;

	do {

		num_bytes_to_read = (need_read_all || BUFFER_SIZE < part_size) ? BUFFER_SIZE : part_size;
		size_t nread = fread(buf, sizeof(char), num_bytes_to_read, f_in);
		if (nread == 0)
			break;

		size_t nwrite = fwrite(buf, sizeof(char), nread, f_out);
		if (nwrite != nread) {
			printf("Cannot write data to file \"%s\"\n", fname_out);
			perror("Error information:");
			cleanup();
			fclose(f_out);
			exit(EXIT_FAILURE);
		}

		total_written_bytes += nwrite;

		if (!need_read_all)
			part_size -= nwrite;

	} while (need_read_all || part_size > 0);

	if (ferror(f_in)) {
		printf("Cannot read data from input file!\n");
		perror("Error information:");
		cleanup();
		fclose(f_out);
		exit(EXIT_FAILURE);
	}

	int close_result = fclose(f_out);
	if (close_result != 0) {
		printf("Cannot close file \"%s\"\n", fname_out);
		perror("Error information:");
		cleanup();
		exit(EXIT_FAILURE);
	}

	printf("Created file \"%s\", size = %d byte(s)\n", fname_out, total_written_bytes);

}

int main(int argc, char** argv)
{

	int i = 0;

	if (argc < 2) {
		char* prg_name = NULL;
#ifdef __linux__
		prg_name = strrchr(argv[0], '/');
#else
		prg_name = strrchr(argv[0], '\\');
#endif
		if (prg_name != NULL)
			++prg_name; // skipping '/' or '\\'
		else
			prg_name = argv[0];

		printf("Utility to split file into pieces of a given size\n");
		printf("Usage:\n");
		printf("%s <input-file> [size-of-part-1] ... [size-of-part-N]\n", prg_name);
		exit(EXIT_FAILURE);
	}

	// checking parameters
	for (i = 2; i < argc; ++i) {
		char* cur_param_val = argv[i];
		long int cur_size = str2longint(cur_param_val);
		if (cur_size == 0) 	{
			printf("Parameter #%d (\"%s\") is invalid (not a valid number)\n", i - 1, cur_param_val);
			exit(EXIT_FAILURE);
		}
	}

	char* filename_in = argv[1];
	f_in = fopen(filename_in, "rb");
	
	if (f_in == NULL) {
		printf("Cannot open file \"%s\"\n", filename_in);
		perror("Error information:");
		exit(EXIT_FAILURE);
	}

	int part_num = 0;

	for (i = 2; i < argc; ++i) {
		char* cur_param_val = argv[i];
		long int cur_size = str2longint(cur_param_val);
		if (cur_size == 0) {
			printf("Parameter #%d (\"%s\") is invalid (not a valid number)\n", i - 1, cur_param_val);
			cleanup();
			exit(EXIT_FAILURE);
		}

		if (!feof(f_in)) {
			++part_num;
			generate_filename_out(filename_in, part_num);
			makepart(f_in, filename_out, cur_size);
		}
		else
			break;
	}


	if (!feof(f_in)) {
		++part_num;
		generate_filename_out(filename_in, part_num);
		makepart(f_in, filename_out, 0);
	}

	cleanup();
	
	printf("SUCESS!!!\n");

	return EXIT_SUCCESS;

}
